# Training catalogue

```{note}
This catalogue is under construction! 
Courses are currently sorted by date, other sorting options will be available. 
```

````{panels}
:column: col-12
:card: border-2
2022-1_DTC1
^^^
**Exploratory Analysis of Biological Data: Data Carpentry**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Data management and curation,badge-success` {badge}`Transcriptomics,badge-success`
```{dropdown} More details:
- Course name: Exploratory Analysis of Biological Data: Data Carpentry
- Instructors: Renato Alves, Gaurav Diwan, Florian Huber, Ece Kartal, Supriya Khedkar, Fotis E. Psomopoulos, Hugo Tavares
- Helpers: Lisanna Paladin, Christian Schudoma
- Number of attendees: 28
- Year of training: 2022
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://unode.github.io/2022-01-31-heidelberg-online/)
- Date: 2022-01-31
- Duration (days): 5
```
---
2021-11_DL1
^^^
**Introduction to deep learning**

{badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Statistics and machine learning,badge-warning`
```{dropdown} More details:
- Course name: Introduction to deep learning
- Instructors: Colin Sauze,Dominik Kutra,Lazlo Dobson
- Helpers: Renato Alves,Lisanna Paladin
- Number of attendees: 20
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-deep-learning/)
- Date: 2021-11-29
- Duration (days): 3
```
---
2021-11_GITW1
^^^
**Git week**

{badge}`Using specialised research software,badge-success` {badge}`Data management and curation,badge-success` {badge}`Software project management,badge-success` {badge}`Web technologies,badge-success` {badge}`Software project management,badge-warning` {badge}`Software project management,badge-danger`
```{dropdown} More details:
- Course name: Git week
- Instructors: Renato Alves,Lisanna Paladin,Josep Moscardo,Martin Larralde,Jelle Scholtalbers,Matthias Monfort,Framcesco Tabaro,Jure Pecar,Dominik Kutra,Jean-Karim Heriche,Christian Schudoma
- Number of attendees: 45
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/git-week/)
- Date: 2021-11-15
- Duration (days): 5
```
---
2021-10_PATHDB1
^^^
**Pathogen research through VEuPathDB resources**

{badge}`Using specialised research software,badge-success` {badge}`Proteomics and protein analysis,badge-success`
```{dropdown} More details:
- Course name: Pathogen research through VEuPathDB resources
- Instructors: Jesús Alvarado Valverde
- Helpers: Renato Alves
- Number of attendees: 16
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://oc.embl.de/index.php/f/24381378), [link](https://bio-it.embl.de/events/veupathdb-2021/)
- Date: 2021-10-11
- Duration (days): 1
```
---
2021-9_STATB1
^^^
**Biostatistical Basics**

{badge}`Using specialised research software,badge-success` {badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success`
```{dropdown} More details:
- Course name: Biostatistical Basics
- Instructors: Sarah Kaspar
- Helpers: Julia Philipp, Nils Kurzawa, Junyan Lu
- Number of attendees: 30
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/biostatistical-basics-2021/), [link](https://www.huber.embl.de/users/kaspar/biostat_2021/)
- Date: 2021-09-14
- Duration (days): 4
```
---
2021-2_IMGA1
^^^
**Basics of bioimage analysis**

{badge}`Image analysis,badge-success`
```{dropdown} More details:
- Course name: Basics of bioimage analysis
- Instructors: Christian Tischer, Antonio Politi
- Number of attendees: 40
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/basics-of-bioimage-analysis/)
- Date: 2021-06-07
- Duration (days): 4
```
---
2021-6_MODKIN1
^^^
**Introduction to quantitative kinetic modelling**

{badge}`Biological modelling,badge-success`
```{dropdown} More details:
- Course name: Introduction to quantitative kinetic modelling
- Instructors: Eva Geissen
- Number of attendees: 9
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/iqkm_2021/)
- Date: 2021-06-06
- Duration (days): 4
```
---
2021-5_ONTO1
^^^
**Ontologies – statistics, biases, tools, networks, and interpretation**

{badge}`Using specialised research software,badge-success` {badge}`Programming languages,badge-success` {badge}`Biological networks analysis,badge-success` {badge}`Genomics and comparative genomics,badge-success` {badge}`Proteomics and protein analysis,badge-success` {badge}`Using specialised research software,badge-warning` {badge}`Programming languages,badge-warning`
```{dropdown} More details:
- Course name: Ontologies – statistics, biases, tools, networks, and interpretation
- Instructors: Matt Rogon,Balint Meszaros
- Helpers: Renato Alves
- Number of attendees: 40
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/ontologies-statistics-biases-tools-networks-and-interpretation-2021/), [link](https://oc.embl.de/index.php/f/22221713)
- Date: 2021-05-17
- Duration (days): 4
```
---
2021-3_NETM1
^^^
**Introduction to mining public adtabases on the Web and wuth Cytoscape**

{badge}`Using specialised research software,badge-success` {badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Biological networks analysis,badge-success`
```{dropdown} More details:
- Course name: Introduction to mining public adtabases on the Web and wuth Cytoscape
- Instructors: Matt Rogon
- Number of attendees: 36
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/cbna-2021-l1p2/)
- Date: 2021-03-26
- Duration (days): 1
```
---
2021-3_MATLAB1
^^^
**Introductory course on MATLAB**

{badge}`Programming languages,badge-success`
```{dropdown} More details:
- Course name: Introductory course on MATLAB
- Instructors: Eva Geissen
- Number of attendees: 16
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/matlab_virtual_2021)
- Date: 2021-03-22
- Duration (days): 3
```
---
2021-3_GLXY1
^^^
**Analysis of Data with Galaxy**

{badge}`Using specialised research software,badge-success` {badge}`Computational workflow management,badge-success`
```{dropdown} More details:
- Course name: Analysis of Data with Galaxy
- Instructors: Charles Girardot,Jelle Scholtalbers,Matthias Monfort,Jean-Karim Heriche,Christian Tischer,Nicolas Descostes,Renato Alves
- Number of attendees: 32
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/analysis-of-data-with-galaxy-2021/), [link](https://oc.embl.de/index.php/f/22271382)
- Date: 2021-03-18
- Duration (days): 2
```
---
2021-3_CHIMRX1
^^^
**Learning to visualise molecular structures with ChimeraX**

{badge}`Using specialised research software,badge-success` {badge}`Structural biology,badge-success`
```{dropdown} More details:
- Course name: Learning to visualise molecular structures with ChimeraX
- Instructors: Toby Gibson,Balint Meszaros,Manjeet Kumar
- Helpers: Renato Alves
- Number of attendees: 22
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/chimerax-2021/)
- Date: 2021-03-15
- Duration (days): 1
```
---
2021-3_JEKGH1
^^^
**Building Websites With GitHub and Jekyll**

{badge}`Programming languages,badge-success` {badge}`Web technologies,badge-success`
```{dropdown} More details:
- Course name: Building Websites With GitHub and Jekyll
- Instructors: Renato Alves,Sarah Stevens,Aleks Nenadic,Toby Hodges,Anne Fouilloux
- Number of attendees: 20
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/jekyll-github-pilot-2021/)
- Date: 2021-03-11
- Duration (days): 3
```
---
2021-2_MORPH1
^^^
**Morpheus tutorial - Modelling and simulation of multicellular dynamics and tissue morphogenesis**

{badge}`Biological modelling,badge-success`
```{dropdown} More details:
- Course name: Morpheus tutorial - Modelling and simulation of multicellular dynamics and tissue morphogenesis
- Instructors: Eva Geissen, Jörn Starruß, Lutz Brusch, Robert Müller, Diego Jahn, Sebastian Gonzalez-Tirado
- Number of attendees: 20
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/morpheus-tutorial/)
- Date: 2021-02-26
- Duration (days): 1
```
---
2021-2_NETB1
^^^
**Introduction to Data Analysis and Network Biology with Cytoscape**

{badge}`Using specialised research software,badge-success` {badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Biological networks analysis,badge-success` {badge}`Proteomics and protein analysis,badge-warning`
```{dropdown} More details:
- Course name: Introduction to Data Analysis and Network Biology with Cytoscape
- Instructors: Matt Rogon
- Number of attendees: 36
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/cbna-introduction-to-cytoscape-and-network-biology/)
- Date: 2021-02-25
- Duration (days): 1
```
---
2021-2_RTIDY1
^^^
**Data Handling and Visualization with R**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Command-line computing,badge-success`
```{dropdown} More details:
- Course name: Data Handling and Visualization with R
- Instructors: Mike Smith,Renato Alves,Constantin Ahlmann-Eltze,Nicolas Descostes
- Helpers: Jakob Wirbel, Sarah Kaspar, Wasiu Akanni
- Number of attendees: 27
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-r-2021-02/), [link](https://descostesn.github.io/RIntroProgBio/solutions.html)
- Date: 2021-02-23
- Duration (days): 4
```
---
2021-2_IMGJ3
^^^
**Advanced ImageJ: Batch processing & Macros**

{badge}`Using specialised research software,badge-success` {badge}`Image analysis,badge-success`
```{dropdown} More details:
- Course name: Advanced ImageJ: Batch processing & Macros
- Instructors: Christian Tischer,Robert Haase,Noreen Walker,Gayathri Nadar
- Number of attendees: 10
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/advanced-imagej-batch-processing-macros/)
- Date: 2021-02-22
- Duration (days): 3
```
---
2021-1_IMGA1
^^^
**Basics of bioimage analysis**

{badge}`Image analysis,badge-success`
```{dropdown} More details:
- Course name: Basics of bioimage analysis
- Instructors: Christian Tischer, Antoino Politi
- Number of attendees: 40
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/basics-of-bioimage-analysis/)
- Date: 2021-01-25
- Duration (days): 4
```
---
2021-1_DTC1
^^^
**Exploratory Analysis of Biological Data: Data Carpentry**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Data management and curation,badge-success` {badge}`Transcriptomics,badge-success`
```{dropdown} More details:
- Course name: Exploratory Analysis of Biological Data: Data Carpentry
- Instructors: Renato Alves,Florian Huber,Thea Van Rossum,Hugo Tavares,Fotis Psomopoulos,Gaurav Diwan
- Helpers: Ece Kartal, Christian Schudoma
- Number of attendees: 26
- Year of training: 2021
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://unode.github.io/2021-01-25-heidelberg/)
- Date: 2021-01-25
- Duration (days): 5
```
---
2020-11_MSAPRT1
^^^
**Making inferences from multiple sequence alignments of modular proteins**

{badge}`Using specialised research software,badge-success` {badge}`Proteomics and protein analysis,badge-success` {badge}`Structural biology,badge-success`
```{dropdown} More details:
- Course name: Making inferences from multiple sequence alignments of modular proteins
- Instructors: Toby Gibson
- Helpers: Renato Alves
- Number of attendees: 12
- Year of training: 2020
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/making-inferences-from-multiple-sequence-alignments-of-modular-proteins/), [link](https://oc.embl.de/index.php/f/20805919)
- Date: 2020-11-05
- Duration (days): 1
```
---
2020-10_ONTO1
^^^
**Ontologies – statistics, biases, tools, networks, and interpretation**

{badge}`Using specialised research software,badge-success` {badge}`Programming languages,badge-success` {badge}`Biological networks analysis,badge-success` {badge}`Genomics and comparative genomics,badge-success` {badge}`Proteomics and protein analysis,badge-success` {badge}`Using specialised research software,badge-warning` {badge}`Programming languages,badge-warning`
```{dropdown} More details:
- Course name: Ontologies – statistics, biases, tools, networks, and interpretation
- Instructors: Matt Rogon,Balint Meszaros
- Helpers: Renato Alves
- Number of attendees: 51
- Year of training: 2020
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/ontologies-statistics-biases-tools-networks-and-interpretation/), [link](https://oc.embl.de/index.php/f/22221710)
- Date: 2020-10-26
- Duration (days): 4
```
---
2020-10_SWC1
^^^
**Computing Skills for Reproducible Research: Software Carpentry Course 2020**

{badge}`Programming languages,badge-success` {badge}`Cluster computing,badge-success` {badge}`Command-line computing,badge-success` {badge}`Computational workflow management,badge-success` {badge}`Software project management,badge-success`
```{dropdown} More details:
- Course name: Computing Skills for Reproducible Research: Software Carpentry Course 2020
- Instructors: Frank Thommen,Holger Dinkel,Kimberly Meechan,Mike Smith,Renato Alves,Thea Van Rossum,Thomas Schwarzl,Toby Hodges
- Helpers: Christian Schudoma, Melanie Kaeser, Nikolaos Papadopoulos
- Number of attendees: 20
- Year of training: 2020
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://unode.github.io/2020-10-19-heidelberg/)
- Date: 2020-10-19
- Duration (days): 5
```
---
2020-7_PYTH2-2
^^^
**Intermediate Python Follow-Up: Plotting**

{badge}`Exploratory data analysis and visualisation,badge-warning`
```{dropdown} More details:
- Course name: Intermediate Python Follow-Up: Plotting
- Instructors: Toby Hodges,Renato Alves, Wasiu Akanni, Kimberly Meechan, Nikolaos Papadopoulos, Jelle Scholtalbers
- Number of attendees: 70
- Year of training: 2020
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/intermediate-python-follow-up-plotting/), [link](https://oc.embl.de/index.php/f/20171176)
- Date: 2020-07-21
- Duration (days): 1
```
---
2020-7_PYTH2
^^^
**Intermediate Python Programming**

{badge}`Exploratory data analysis and visualisation,badge-warning` {badge}`Programming languages,badge-warning`
```{dropdown} More details:
- Course name: Intermediate Python Programming
- Instructors: Toby Hodges,Renato Alves, Wasiu Akanni, Kimberly Meechan, Nikolaos Papadopoulos, Jelle Scholtalbers
- Number of attendees: 70
- Year of training: 2020
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/intermediate-python-programming-2/), [link](https://oc.embl.de/index.php/f/20171176)
- Date: 2020-07-06
- Duration (days): 6
```
---
2020-6_CELLP1
^^^
**Essentials of Batch Image Analysis with Cell Profiler**

{badge}`Image analysis,badge-success`
```{dropdown} More details:
- Course name: Essentials of Batch Image Analysis with Cell Profiler
- Instructors: Christian Tischer
- Number of attendees: 20
- Year of training: 2020
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/cellprofiler-essentials/)
- Date: 2020-06-25
- Duration (days): 1
```
---
2020-5_NETM2
^^^
**Mining biomedical data with networks (part 2)**

{badge}`Biological networks analysis,badge-warning`
```{dropdown} More details:
- Course name: Mining biomedical data with networks (part 2)
- Instructors: Matt Rogon
- Number of attendees: 17
- Year of training: 2020
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/cbna-2020-part-2-mining-biomedical-data-with-networks-kegg-drugs-diseases/)
- Date: 2020-05-14
- Duration (days): 1
```
---
2020-5_RTIDY1
^^^
**Introduction to R**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Command-line computing,badge-success`
```{dropdown} More details:
- Course name: Introduction to R
- Instructors: Thea Van Rossum,Mike Smith,Toby Hodges,Renato Alves
- Number of attendees: 72
- Year of training: 2020
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-r/)
- Date: 2020-05-05
- Duration (days): 4
```
---
2020-4_NETM1
^^^
**Mining biomedical data with networks**

{badge}`Using specialised research software,badge-warning` {badge}`Exploratory data analysis and visualisation,badge-warning` {badge}`Biological networks analysis,badge-warning`
```{dropdown} More details:
- Course name: Mining biomedical data with networks
- Instructors: Matt Rogon
- Number of attendees: 36
- Year of training: 2020
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/cbna-course-series-2020-part-2/)
- Date: 2020-04-29
- Duration (days): 1
```
---
2020-4_NETB1
^^^
**Introduction to Cytoscape and Network Biology**

{badge}`Using specialised research software,badge-success` {badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Biological networks analysis,badge-success` {badge}`Proteomics and protein analysis,badge-warning`
```{dropdown} More details:
- Course name: Introduction to Cytoscape and Network Biology
- Instructors: Matt Rogon, Manjeet Kumar
- Number of attendees: 20
- Year of training: 2020
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-cytoscape-cbna-bio-it/)
- Date: 2020-04-01
- Duration (days): 1
```
---
2020-3_IMGA1
^^^
**Basics of image analysis**

{badge}`Image analysis,badge-success`
```{dropdown} More details:
- Course name: Basics of image analysis
- Instructors: Christian Tischer
- Number of attendees: 20
- Year of training: 2020
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/course-basics-of-image-analysis-2020/)
- Date: 2020-03-30
- Duration (days): 2
```
---
2020-3_MSMBBC
^^^
**Introduction to quantitative kinetic modelling**

{badge}`Statistics and machine learning,badge-success`
```{dropdown} More details:
- Course name: Introduction to quantitative kinetic modelling
- Instructors: Eva Geissen, Toby Hodges, Matt Rogon
- Number of attendees: 70
- Year of training: 2020
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/msmb-book-club/)
- Date: 2020-03-27
- Duration (days): 1
```
---
2020-3_PYTH1
^^^
**Introduction to Python Programming**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Data management and curation,badge-success`
```{dropdown} More details:
- Course name: Introduction to Python Programming
- Instructors: Toby Hodges,Renato Alves,Wasiu Akanni,Kimberly Meechan,Nikolaus Papadopoulos,Jelle Sholtalbers
- Number of attendees: 70
- Year of training: 2020
- Location: Online
- Format (In-person/Virtual/...): Virtual
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-python-programming-5/)
- Date: 2020-03-23
- Duration (days): 2
```
---
2020-3_REGEXP1
^^^
**Introduction to Regular Expressions**

{badge}`Exploratory data analysis and visualisation,badge-warning` {badge}`Programming languages,badge-warning` {badge}`Command-line computing,badge-warning`
```{dropdown} More details:
- Course name: Introduction to Regular Expressions
- Instructors: Toby Hodges,Supriya Khedkar
- Helpers: Renato Alves
- Number of attendees: 22
- Year of training: 2020
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-regular-expressions-2/), [link](https://grp-bio-it-workshops.embl-community.io/regex-introduction/)
- Date: 2020-03-18
- Duration (days): 1
```
---
2020-2_IMG3DV1
^^^
**3D image visualisation using 3D viewing software**

{badge}`Image analysis,badge-success`
```{dropdown} More details:
- Course name: 3D image visualisation using 3D viewing software
- Instructors: Lucas Schuetz
- Number of attendees: 20
- Year of training: 2020
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/3d-image-visualization-using-3d-viewing-software/)
- Date: 2020-02-28
- Duration (days): 1
```
---
2020-2_IMG3DP1
^^^
**Essential of 3D Image Inspection and Presentation**

{badge}`Image analysis,badge-success`
```{dropdown} More details:
- Course name: Essential of 3D Image Inspection and Presentation
- Instructors: Christian Tischer
- Number of attendees: 20
- Year of training: 2020
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/course-essentials-of-3d-image-data-inspection-and-presentation/)
- Date: 2020-02-20
- Duration (days): 1
```
---
2020-2_METAG1
^^^
**Metagenomic Bioinformatics Analysis**

{badge}`Metagenomics and other meta-omics,badge-success` {badge}`Using specialised research software,badge-warning` {badge}`Exploratory data analysis and visualisation,badge-warning`
```{dropdown} More details:
- Course name: Metagenomic Bioinformatics Analysis
- Instructors: Laura Carroll,Nicolai Karcher,Alessio Milanese,Jakob Wirbel,Georg Zeller
- Number of attendees: 23
- Year of training: 2020
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/metagenomic-bioinformatics-analysis/)
- Date: 2020-02-18
- Duration (days): 2
```
---
2020-1_DTC1
^^^
**Data Carpentry Workshop 2020**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Data management and curation,badge-success` {badge}`Transcriptomics,badge-success`
```{dropdown} More details:
- Course name: Data Carpentry Workshop 2020
- Instructors: Toby Hodges,Florian Huber,Fotis E. Psomopoulos
- Helpers: Malvika Sharan, Georg Zeller, Thea Van Rossum, Gaurav Diwan, Nicolai Karcher, Renato Alves
- Number of attendees: 28
- Year of training: 2020
- Location: Training Lab
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://tobyhodges.github.io/2020-01-28-heidelberg/)
- Date: 2020-01-28
- Duration (days): 3
```
---
2020-1_IMGHP1
^^^
**Essential of Image Data Inspection, Handling and Presentation**

{badge}`Image analysis,badge-success`
```{dropdown} More details:
- Course name: Essential of Image Data Inspection, Handling and Presentation
- Instructors: Christian Tischer
- Number of attendees: 20
- Year of training: 2020
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/course-essentials-of-image-data-inspection-handling-and-presentation/)
- Date: 2020-01-27
- Duration (days): 1
```
---
2020-1_PYIMG1
^^^
**Image Analysis with Python**

{badge}`Image analysis,badge-success` {badge}`Image analysis,badge-warning`
```{dropdown} More details:
- Course name: Image Analysis with Python
- Instructors: Dominik Kutra,Gregor Mönke
- Number of attendees: 20
- Year of training: 2020
- Location: ATC Courtyard
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/dc-image-processing-python/)
- Date: 2020-01-14
- Duration (days): 2
```
---
2020-1_PYTH1
^^^
**Introduction to Python**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Data management and curation,badge-success`
```{dropdown} More details:
- Course name: Introduction to Python
- Instructors: Mattia Forneris,Toby Hodges,Nic Karcher
- Number of attendees: 28
- Year of training: 2020
- Location: ATC Courtyard
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-python-programming-4/)
- Date: 2020-01-08
- Duration (days): 2
```
---
2019-12_GIT2
^^^
**Version Control with Git & git.embl.de**

{badge}`Command-line computing,badge-success` {badge}`Data management and curation,badge-success` {badge}`Software project management,badge-success`
```{dropdown} More details:
- Course name: Version Control with Git & git.embl.de
- Instructors: Toby Hodges,Nicolas Descostes
- Number of attendees: 37
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/version-control-with-git-git-embl-de-2/)
- Date: 2019-12-10
- Duration (days): 1
```
---
2019-11_MLR1
^^^
**Machine Learning in R (mlr workshop)**

{badge}`Statistics and machine learning,badge-warning`
```{dropdown} More details:
- Course name: Machine Learning in R (mlr workshop)
- Instructors: Bernd Bischl,Martin Binder,Giuseppe Casalicchio,Georg Zeller
- Helpers: Malvika Sharan
- Number of attendees: 40
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/machine-learning-in-r-2019/)
- Date: 2019-11-06
- Duration (days): 2
```
---
2019-10_MODKIN1
^^^
**Introduction to quantitative kinetic modelling**

{badge}`Biological modelling,badge-success`
```{dropdown} More details:
- Course name: Introduction to quantitative kinetic modelling
- Instructors: Eva Geissen
- Number of attendees: 8
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-quantitative-kinetic-modelling-2019/)
- Date: 2019-10-24
- Duration (days): 2
```
---
2019-10_SWC1
^^^
**Software Carpentry**

{badge}`Programming languages,badge-success` {badge}`Cluster computing,badge-success` {badge}`Command-line computing,badge-success` {badge}`Computational workflow management,badge-success` {badge}`Software project management,badge-success`
```{dropdown} More details:
- Course name: Software Carpentry
- Instructors: Renato Alves,Holger Dinkel,Toby Hodges,Tom Schwarzl,Malvika Sharan,Frank Thommen
- Number of attendees: 28
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://malvikasharan.github.io/2019-10-16-heidelberg/)
- Date: 2019-10-16
- Duration (days): 3
```
---
2019-10_STATML1
^^^
**Statistics for Machine learning**

{badge}`Statistics and machine learning,badge-success`
```{dropdown} More details:
- Course name: Statistics for Machine learning
- Instructors: Anna Kreshuk
- Helpers: Malvika Sharan
- Number of attendees: 40
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/machine-learning-discussion-workshop-2019/), [link](https://introduction-to-machine-learning.netlify.app/)
- Date: 2019-10-14
- Duration (days): 1
```
---
2019-9_IMGHP1
^^^
**Basics of image data handling and presentation**

{badge}`Image analysis,badge-success`
```{dropdown} More details:
- Course name: Basics of image data handling and presentation
- Instructors: Christian Tischer
- Number of attendees: 20
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/course-basics-of-image-data-handling-and-presentation/)
- Date: 2019-09-26
- Duration (days): 1
```
---
2019-9_NETA2
^^^
**Intermediate Network Analysis and visualisation**

{badge}`Using specialised research software,badge-warning` {badge}`Programming languages,badge-warning` {badge}`Biological networks analysis,badge-warning`
```{dropdown} More details:
- Course name: Intermediate Network Analysis and visualisation
- Instructors: Matt Rogon
- Number of attendees: 7
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/intermediate-network-analysis-and-visualisation-r-igraph-cytoscape-2019/)
- Date: 2019-09-11
- Duration (days): 3
```
---
2019-9_MATLAB1
^^^
**Introductory course on MATLAB**

{badge}`Programming languages,badge-success`
```{dropdown} More details:
- Course name: Introductory course on MATLAB
- Instructors: Eva Geissen
- Number of attendees: 10
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introductory-course-on-programming-with-matlab-2/)
- Date: 2019-09-10
- Duration (days): 2
```
---
2019-6_ILASTK1
^^^
**Image analysis and processing by Ilastik**

{badge}`Image analysis,badge-success`
```{dropdown} More details:
- Course name: Image analysis and processing by Ilastik
- Instructors: Anna Kreshuk,Dominik Kutra
- Number of attendees: 20
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/image-processing-analysis-with-ilastik/)
- Date: 2019-06-27
- Duration (days): 1
```
---
2019-6_KNIME1
^^^
**Image analysis in KNIME**

{badge}`Image analysis,badge-success` {badge}`Image analysis,badge-warning` {badge}`Image analysis,badge-danger`
```{dropdown} More details:
- Course name: Image analysis in KNIME
- Instructors: Stefan Helfrich
- Number of attendees: 20
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/basics-of-knime-basics-of-image-analysis-in-knime/), [link](https://bio-it.embl.de/events/advanced-image-analysis-in-knime/), [link](https://bio-it.embl.de/events/knime-image-analysis-integration-work-on-own-data/)
- Date: 2019-06-17
- Duration (days): 3
```
---
2019-5_ONTO1
^^^
**Introduction to Ontologies, Pathways and Networks**

{badge}`Using specialised research software,badge-success` {badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Biological networks analysis,badge-success`
```{dropdown} More details:
- Course name: Introduction to Ontologies, Pathways and Networks
- Instructors: Matt Rogon
- Number of attendees: 12
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-ontologies-pathways-and-networks-2019/)
- Date: 2019-05-27
- Duration (days): 3
```
---
2019-5_GIT2
^^^
**Intermediate version control with Git & git.embl.de**

{badge}`Software project management,badge-success` {badge}`Using specialised research software,badge-warning` {badge}`Command-line computing,badge-warning`
```{dropdown} More details:
- Course name: Intermediate version control with Git & git.embl.de
- Instructors: Marc Gouw,Toby Hodges,Jelle Scholtalbers
- Number of attendees: 22
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/intermediate-git-git-embl-de/)
- Date: 2019-05-23
- Duration (days): 1
```
---
2019-5_PYTH1
^^^
**Python Programming**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Data management and curation,badge-success`
```{dropdown} More details:
- Course name: Python Programming
- Instructors: Gregor Moenke,Nicolas Descostes
- Helpers: Toby Hodges,Karin Sasaki
- Number of attendees: 10
- Year of training: 2019
- Location: EMBL Rome
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://git.embl.de/moenke/ITPP)
- Date: 2019-05-01
- Duration (days): 1
```
---
2019-4_PYTH1
^^^
**Introduction to Python Programming**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Data management and curation,badge-success`
```{dropdown} More details:
- Course name: Introduction to Python Programming
- Instructors: Malvika Sharan,Mattia Forneris,Gregor Moenke
- Number of attendees: 10
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/python-2019/)
- Date: 2019-04-09
- Duration (days): 2
```
---
2019-4_IMGAK1
^^^
**Basics of image analysis**

{badge}`Image analysis,badge-success` {badge}`Image analysis,badge-warning`
```{dropdown} More details:
- Course name: Basics of image analysis
- Instructors: Christian Tischer
- Number of attendees: 20
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/course-basics-of-image-analysis/)
- Date: 2019-04-08
- Duration (days): 3
```
---
2019-3_SQL1
^^^
**Introduction to SQL**

{badge}`Programming languages,badge-success`
```{dropdown} More details:
- Course name: Introduction to SQL
- Instructors: Toby Hodges
- Number of attendees: 7
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-sql/)
- Date: 2019-03-21
- Duration (days): 1
```
---
2019-3_HPC2
^^^
**Intermediate Cluster Computing**

{badge}`Cluster computing,badge-warning`
```{dropdown} More details:
- Course name: Intermediate Cluster Computing
- Instructors: Jure Pecar
- Helpers: Toby Hodges
- Number of attendees: 19
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/intermediate-cluster-computing-2/)
- Date: 2019-03-20
- Duration (days): 1
```
---
2019-3_HPC1
^^^
**Introduction to Cluster Computing**

{badge}`Cluster computing,badge-success`
```{dropdown} More details:
- Course name: Introduction to Cluster Computing
- Instructors: Toby Hodges
- Helpers: Jure Pecar
- Number of attendees: 18
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-cluster-computing-at-embl/)
- Date: 2019-03-19
- Duration (days): 1
```
---
2019-2_SING1
^^^
**Singularity Workshop**

{badge}`Cluster computing,badge-warning` {badge}`Software project management,badge-warning`
```{dropdown} More details:
- Course name: Singularity Workshop
- Instructors: Michael Hall,Josep Moscardo
- Helpers: Toby Hodges
- Number of attendees: 34
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-container-computing-with-singularity/), [link](https://git.embl.de/grp-bio-it/singularity-training-2019)
- Date: 2019-02-27
- Duration (days): 1
```
---
2019-2_CMD2
^^^
**Intermediate Commandline**

{badge}`Command-line computing,badge-success`
```{dropdown} More details:
- Course name: Intermediate Commandline
- Instructors: Jean-Karim Heriche,Toby Hodges
- Number of attendees: 18
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/intermediate-linux-command-line-basic-scripting-4/)
- Date: 2019-02-13
- Duration (days): 1
```
---
2019-2_CMD1
^^^
**Introduction to Commandline**

{badge}`Command-line computing,badge-success`
```{dropdown} More details:
- Course name: Introduction to Commandline
- Instructors: Malvika Sharan,Marc Gouw
- Helpers: Toby Hodges
- Number of attendees: 16
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-command-line-computing-3/)
- Date: 2019-02-12
- Duration (days): 1
```
---
2019-2_DTMA1
^^^
**Data Management Application**

{badge}`Data management and curation,badge-success`
```{dropdown} More details:
- Course name: Data Management Application
- Instructors: William Murphy
- Helpers: Malvika Sharan
- Number of attendees: 14
- Year of training: 2019
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/embl-data-management-application-hands-on-workshop/)
- Date: 2019-02-01
- Duration (days): 1
```
---
2019-1_DTC1
^^^
**Data Carpentry**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Data management and curation,badge-success` {badge}`Transcriptomics,badge-success`
```{dropdown} More details:
- Course name: Data Carpentry
- Instructors: Hugo Tavares,Florian Huber,Thea van Rossum,Georg Zeller
- Helpers: Toby Hodges,Malvika Sharan
- Number of attendees: 27
- Year of training: 2019
- Location: EMBl Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://tavareshugo.github.io/2019-01-29-EMBL/)
- Date: 2019-01-29
- Duration (days): 3
```
---
2018-11_MODLOG1
^^^
**Introduction to qualitative logical modelling**

{badge}`Biological modelling,badge-success`
```{dropdown} More details:
- Course name: Introduction to qualitative logical modelling
- Instructors: Eva Geissen
- Number of attendees: 6
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/1-5-days-introduction-to-qualitative-logical-modelling-2018/)
- Date: 2018-11-20
- Duration (days): 2
```
---
2018-10_CMD1+PYTH1
^^^
**Introduction to Unix and Python Programming**

{badge}`Programming languages,badge-success` {badge}`Command-line computing,badge-success`
```{dropdown} More details:
- Course name: Introduction to Unix and Python Programming
- Instructors: Malvika Sharan
- Number of attendees: 2
- Year of training: 2018
- Location: EMBL Monterotondo
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-python-programming-rome/)
- Date: 2018-10-29
- Duration (days): 2
```
---
2018-10_SWC1
^^^
**Software Carpentry**

{badge}`Programming languages,badge-success` {badge}`Command-line computing,badge-success` {badge}`Computational workflow management,badge-success` {badge}`Software project management,badge-success`
```{dropdown} More details:
- Course name: Software Carpentry
- Instructors: Toby Hodges,Mike Smith,Tom Schwarzl,Frank Thommen,Holger Dinkel
- Helpers: Christian Arnold,Juliana Glavina,Thea Van Rossum,Claudia Beleites,Georg Zeller
- Number of attendees: 26
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://tobyhodges.github.io/2018-10-16-heidelberg/)
- Date: 2018-10-01
- Duration (days): 1
```
---
2018-9_ONTO1
^^^
**Introduction to Ontologies, Pathways and Networks**

{badge}`Using specialised research software,badge-success` {badge}`Programming languages,badge-success` {badge}`Biological networks analysis,badge-success` {badge}`Genomics and comparative genomics,badge-success` {badge}`Proteomics and protein analysis,badge-success` {badge}`Using specialised research software,badge-warning` {badge}`Programming languages,badge-warning`
```{dropdown} More details:
- Course name: Introduction to Ontologies, Pathways and Networks
- Instructors: Matt Rogon
- Number of attendees: 15
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-ontologies-pathways-and-networks-2018/)
- Date: 2018-09-12
- Duration (days): 3
```
---
2018-9_DTAPY1
^^^
**Introduction to Data Analysis with Python**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success`
```{dropdown} More details:
- Course name: Introduction to Data Analysis with Python
- Instructors: Malvika Sharan,Marc Gouw,Toby Hodges
- Number of attendees: 20
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/python-course-for-beginners-sep-2018/)
- Date: 2018-09-06
- Duration (days): 2
```
---
2018-7_MATLAB1
^^^
**Introduction to programming with MATLAB**

{badge}`Programming languages,badge-success`
```{dropdown} More details:
- Course name: Introduction to programming with MATLAB
- Instructors: Eva-Maria Geissen
- Number of attendees: 8
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introductory-course-on-programming-with-matlab/)
- Date: 2018-07-17
- Duration (days): 2
```
---
2018-6_CMD1
^^^
**Introduction to the Linux Command Line and Scripting**

{badge}`Cluster computing,badge-success` {badge}`Cluster computing,badge-warning`
```{dropdown} More details:
- Course name: Introduction to the Linux Command Line and Scripting
- Instructors: Malvika Sharan,Frank Thommen
- Number of attendees: 20
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-linux-command-line-basic-scripting/), [link](https://git.embl.de/grp-bio-it/linuxcommandline)
- Date: 2018-06-26
- Duration (days): 2
```
---
2018-6_KNIME1
^^^
**KNIME Image Analysis Basics**

{badge}`Image analysis,badge-success`
```{dropdown} More details:
- Course name: KNIME Image Analysis Basics
- Instructors: Christian Tischer (?)
- Number of attendees: 15
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/workshop-knime-image-analysis-basics/)
- Date: 2018-06-21
- Duration (days): 1
```
---
2018-6_HPC2
^^^
**Intermediate Cluster Computing at EMBL**

{badge}`Cluster computing,badge-warning`
```{dropdown} More details:
- Course name: Intermediate Cluster Computing at EMBL
- Instructors: Mike Smith,Jure Pečar,Toby Hodges
- Number of attendees: 24
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/intermediate-cluster-computing/)
- Date: 2018-06-19
- Duration (days): 1
```
---
2018-6_NETAGR1
^^^
**Netwok analysis using iGraph and R**

{badge}`Exploratory data analysis and visualisation,badge-warning` {badge}`Programming languages,badge-warning` {badge}`Biological modelling,badge-warning`
```{dropdown} More details:
- Course name: Netwok analysis using iGraph and R
- Instructors: Matt Rogon
- Number of attendees: 10
- Year of training: 2018
- Location: EMBL Monterotondo
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/cbna-monterotondo-module-2-2018/)
- Date: 2018-06-14
- Duration (days): 1
```
---
2018-6_ONTO1
^^^
**Introduction to Ontologies, Pathways and Networks**

{badge}`Using specialised research software,badge-success` {badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Biological networks analysis,badge-success`
```{dropdown} More details:
- Course name: Introduction to Ontologies, Pathways and Networks
- Instructors: Matt Rogon
- Number of attendees: 10
- Year of training: 2018
- Location: EMBL Monterotondo
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/cbna-monterotondo-module-1-2018/)
- Date: 2018-06-13
- Duration (days): 1
```
---
2018-6_MLR1
^^^
**Machine Learning in R (mlr workshop)**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Programming languages,badge-warning` {badge}`Statistics and machine learning,badge-warning`
```{dropdown} More details:
- Course name: Machine Learning in R (mlr workshop)
- Instructors: Bernd Bischl,Xudong Sun,Michel Lang,Bernd Klaus,Georg Zeller
- Number of attendees: 20
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/machine-learning-in-r-advanced-course/)
- Date: 2018-06-12
- Duration (days): 2
```
---
2018-6_IMGAPY1
^^^
**(Image) Data Visualisation and analysis (Month-2 of the series)**

{badge}`Image analysis,badge-warning`
```{dropdown} More details:
- Course name: (Image) Data Visualisation and analysis (Month-2 of the series)
- Instructors: Tobias Rasse,Toby Hodges,Jonas Hartmann,Volker Hilsenstein,Christian Tischer
- Number of attendees: 15
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://git.embl.de/grp-bio-it/image-analysis-with-python)
- Date: 2018-06-06
- Duration (days): 3
```
---
2018-5_MODKIN1
^^^
**Introduction to qualitative logical modelling**

{badge}`Biological modelling,badge-success`
```{dropdown} More details:
- Course name: Introduction to qualitative logical modelling
- Instructors: Eva Geissen
- Number of attendees: 6
- Year of training: 2018
- Location: EMBL Monterotondo
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/cbm-rome-2018-module-2-introduction-to-quantitative-kinetic-modelling/)
- Date: 2018-05-29
- Duration (days): 1
```
---
2018-5_MODLOG1
^^^
**Introduction to qualitative logical modelling**

{badge}`Biological modelling,badge-success`
```{dropdown} More details:
- Course name: Introduction to qualitative logical modelling
- Instructors: Eva Geissen
- Number of attendees: 9
- Year of training: 2018
- Location: EMBL Monterotondo
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/cbm-rome-2018-module-1-introduction-to-qualitative-logical-modelling/)
- Date: 2018-05-28
- Duration (days): 1
```
---
2018-5_HPC1
^^^
**Introduction to Cluster Computing at EMBL**

{badge}`Cluster computing,badge-success`
```{dropdown} More details:
- Course name: Introduction to Cluster Computing at EMBL
- Instructors: Mike Smith,Jure Pečar,Toby Hodges
- Number of attendees: 10
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-the-embl-compute-cluster/), [link](https://oc.embl.de/index.php/f/20171177)
- Date: 2018-05-07
- Duration (days): 1
```
---
2018-5_IMGAPY1
^^^
**Image Analysis with Python (Month-1 of the series)**

{badge}`Image analysis,badge-success`
```{dropdown} More details:
- Course name: Image Analysis with Python (Month-1 of the series)
- Instructors: Tobias Rasse,Toby Hodges,Jonas Hartmann,Volker Hilsenstein,Christian Tischer
- Number of attendees: 15
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://git.embl.de/grp-bio-it/image-analysis-with-python)
- Date: 2018-05-02
- Duration (days): 5
```
---
2018-5_DTC1
^^^
**Data Carpentry**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Data management and curation,badge-success` {badge}`Transcriptomics,badge-success`
```{dropdown} More details:
- Course name: Data Carpentry
- Instructors: Hugo Tavares,Florian Huber
- Helpers: Toby Hodges,Malvika Sharan,Marc Gouw
- Number of attendees: 25
- Year of training: 2018
- Location: EMBl Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://www.embl.de/training/events/2018/DTC18-01/), [link](https://tavareshugo.github.io/2018-05-02-EMBL/)
- Date: 2018-05-02
- Duration (days): 3
```
---
2018-4_STATB1
^^^
**Statistical methods in bioinformatics**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Genomics and comparative genomics,badge-success` {badge}`Statistics and machine learning,badge-warning`
```{dropdown} More details:
- Course name: Statistical methods in bioinformatics
- Instructors: Bernd Klaus
- Number of attendees: 15
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/stat-met-apr-18/)
- Date: 2018-04-25
- Duration (days): 3
```
---
2018-4_GIT2-2
^^^
**Branches & Merge Requests with Git & git.embl.de**

{badge}`Software project management,badge-success`
```{dropdown} More details:
- Course name: Branches & Merge Requests with Git & git.embl.de
- Instructors: Marc Gouw,Toby Hodges
- Number of attendees: 11
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/version-control-with-git-git-embl-de/)
- Notes: We don't have record of how much time was used for this course. Skill assignment assumes one morning.
- Date: 2018-04-24
- Duration (days): 1
```
---
2018-4_MLTIMG1
^^^
**Basics of Multi-dimensional Image Handling and Inspection**

{badge}`Image analysis,badge-success`
```{dropdown} More details:
- Course name: Basics of Multi-dimensional Image Handling and Inspection
- Instructors: Christian Tischer
- Number of attendees: 20
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/cbaalmf-course-basics-of-multi-dimensional-image-handling-and-inspection/)
- Date: 2018-04-19
- Duration (days): 1
```
---
2018-4_GIT2
^^^
**Version Control with Git & git.embl.de**

{badge}`Data management and curation,badge-success` {badge}`Software project management,badge-success`
```{dropdown} More details:
- Course name: Version Control with Git & git.embl.de
- Instructors: Marc Gouw,Toby Hodges
- Number of attendees: 27
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/version-control-with-git-git-embl-de/)
- Date: 2018-04-17
- Duration (days): 1
```
---
2018-4_PYTH1
^^^
**Python for Beginners**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Data management and curation,badge-success`
```{dropdown} More details:
- Course name: Python for Beginners
- Instructors: Malvika Sharan,Marc Gouw
- Number of attendees: 10
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/python-beginners/)
- Date: 2018-04-04
- Duration (days): 2
```
---
2018-2_STATB1
^^^
**Statistical methods in bioinformatics**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Genomics and comparative genomics,badge-success` {badge}`Statistics and machine learning,badge-warning`
```{dropdown} More details:
- Course name: Statistical methods in bioinformatics
- Instructors: Bernd Klaus
- Number of attendees: 4
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/stat-met-feb-18/)
- Date: 2018-02-28
- Duration (days): 4
```
---
2018-2_PRTBIO1
^^^
**Protein Bioinformatics for Beginners**

{badge}`Biological networks analysis,badge-success` {badge}`Proteomics and protein analysis,badge-success` {badge}`Structural biology,badge-success`
```{dropdown} More details:
- Course name: Protein Bioinformatics for Beginners
- Instructors: Malvika Sharan,Marc Gouw,Jelena Calyseva,Manjeet Kumar,Michael Kühn,Toby Gibson
- Number of attendees: 11
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/protein-bioinfo-2018/)
- Date: 2018-02-26
- Duration (days): 3
```
---
2018-2_IMGA1
^^^
**Basic image analysis and inspection**

{badge}`Image analysis,badge-success`
```{dropdown} More details:
- Course name: Basic image analysis and inspection
- Instructors: Christian Tischer
- Number of attendees: 20
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/cba-almf-course-basic-image-handling-and-inspection/)
- Date: 2018-02-22
- Duration (days): 1
```
---
2018-2_CMD2
^^^
**Intemediate Commandline**

{badge}`Command-line computing,badge-success` {badge}`Command-line computing,badge-warning`
```{dropdown} More details:
- Course name: Intemediate Commandline
- Instructors: Jean-Karim Heriche,Marc Gouw
- Number of attendees: 26
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/intermediate-linux-command-line-basic-scripting-3/)
- Date: 2018-02-13
- Duration (days): 3
```
---
2018-1_CMD1
^^^
**Introduction to Commandline**

{badge}`Command-line computing,badge-success`
```{dropdown} More details:
- Course name: Introduction to Commandline
- Instructors: Malvika Sharan,Toby Hodges
- Number of attendees: 29
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-command-line-computing-2/)
- Date: 2018-01-30
- Duration (days): 2
```
---
2018-1_TSEGM1
^^^
**Trainable Image Segmentation**

{badge}`Image analysis,badge-warning`
```{dropdown} More details:
- Course name: Trainable Image Segmentation
- Instructors: Christian Tischer
- Number of attendees: 10
- Year of training: 2018
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/trainable-image-segmentation-workshop/)
- Date: 2018-01-23
- Duration (days): 1
```
---
2018-1_PYTH1
^^^
**Introduction to Python Programming**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Data management and curation,badge-success`
```{dropdown} More details:
- Course name: Introduction to Python Programming
- Instructors: Toby Hodges
- Number of attendees: 11
- Year of training: 2018
- Location: EMBL Grenoble
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-python-programming-3/), [link](https://git.embl.de/grp-bio-it/ITPP)
- Date: 2018-01-17
- Duration (days): 2
```
---
2017-12_STATB1
^^^
**Statistical methods in bioinformatics**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Genomics and comparative genomics,badge-success` {badge}`Statistics and machine learning,badge-warning`
```{dropdown} More details:
- Course name: Statistical methods in bioinformatics
- Instructors: Bernd Klaus
- Number of attendees: 10
- Year of training: 2017
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/stat-bioinf/)
- Date: 2017-12-19
- Duration (days): 3
```
---
2017-10_SWC1
^^^
**Software Carpentry**

{badge}`Programming languages,badge-success` {badge}`Command-line computing,badge-success` {badge}`Computational workflow management,badge-success` {badge}`Software project management,badge-success`
```{dropdown} More details:
- Course name: Software Carpentry
- Instructors: Luis Pedro Coelho,Marc Gouw,Toby Hodges,Thomas Schwarzl,Malvika Sharan,Mike Smith,Frank Thommen,Georg Zeller
- Number of attendees: 30
- Year of training: 2017
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Date: 2017-10-01
- Duration (days): 1
```
---
2017-9_PYTH2
^^^
**Intermediate Python Programming**

{badge}`Exploratory data analysis and visualisation,badge-warning` {badge}`Programming languages,badge-warning` {badge}`Statistics and machine learning,badge-warning`
```{dropdown} More details:
- Course name: Intermediate Python Programming
- Instructors: Marco Galardini,Toby Hodges,Adrien Leger,Jonas Hartmann
- Number of attendees: 21
- Year of training: 2017
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/intermediate-python-programming/)
- Date: 2017-09-19
- Duration (days): 2
```
---
2017-9_MODKIN1
^^^
**Introduction to quantitative kinetic modelling**

{badge}`Biological modelling,badge-success`
```{dropdown} More details:
- Course name: Introduction to quantitative kinetic modelling
- Instructors: Eva Geissen
- Number of attendees: 8
- Year of training: 2017
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-quantitative-kinetic-modelling-2017/)
- Date: 2017-09-14
- Duration (days): 2
```
---
2017-7_PYTH1
^^^
**Introduction to Python programming**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Data management and curation,badge-success`
```{dropdown} More details:
- Course name: Introduction to Python programming
- Number of attendees: N/A
- Year of training: 2017
- Location: IPMB - Heidelberg University
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-python-programming-2/)
- Date: 2017-07-24
- Duration (days): 3
```
---
2017-6_RTIDY1
^^^
**Introduction to R including graphics and data handling**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Command-line computing,badge-success`
```{dropdown} More details:
- Course name: Introduction to R including graphics and data handling
- Instructors: Bernd Klaus
- Number of attendees: 10
- Year of training: 2017
- Location: EMBL Monterotondo
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/a-modern-r-introduction-including-graphics-and-data-handling-monterotondo-17/)
- Date: 2017-06-07
- Duration (days): 5
```
---
2017-6_SWC1
^^^
**Software Carpentry**

{badge}`Programming languages,badge-success` {badge}`Command-line computing,badge-success` {badge}`Computational workflow management,badge-success` {badge}`Software project management,badge-success`
```{dropdown} More details:
- Course name: Software Carpentry
- Instructors: Holger Dinkel,Toby Hodges,Malvika Sharan
- Number of attendees: 30
- Year of training: 2017
- Location: IRI Life Sciences Berlin
- Format (In-person/Virtual/...): In-person
- Date: 2017-06-01
- Duration (days): 1
```
---
2017-5_PYIMG1
^^^
**Image Analysis with Python**

{badge}`Image analysis,badge-warning`
```{dropdown} More details:
- Course name: Image Analysis with Python
- Instructors: Jonas Hartmann,Toby Hodges
- Number of attendees: 20
- Year of training: 2017
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/image-processing-with-python/)
- Date: 2017-05-08
- Duration (days): 3
```
---
2017-3_REGEXP1
^^^
**Introduction to Regular Expressions**

{badge}`Exploratory data analysis and visualisation,badge-warning` {badge}`Programming languages,badge-warning` {badge}`Command-line computing,badge-warning`
```{dropdown} More details:
- Course name: Introduction to Regular Expressions
- Instructors: Markus Fritz,Toby Hodges,Mike Smith
- Number of attendees: 16
- Year of training: 2017
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://grp-bio-it-workshops.embl-community.io/regex-introduction/), [link](https://bio-it.embl.de/events/introduction-to-regular-expressions/)
- Date: 2017-03-23
- Duration (days): 1
```
---
2017-2_PYTH1
^^^
**Introduction to Python Programming**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Data management and curation,badge-success`
```{dropdown} More details:
- Course name: Introduction to Python Programming
- Instructors: Marc Gouw,Toby Hodges,Malvika Sharan
- Number of attendees: 10
- Year of training: 2017
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/introduction-to-python-programming/)
- Date: 2017-02-21
- Duration (days): 2
```
---
2017-1_RTIDY1
^^^
**Introduction to R including graphics and data handling**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Command-line computing,badge-success`
```{dropdown} More details:
- Course name: Introduction to R including graphics and data handling
- Instructors: Bernd Klaus
- Number of attendees: 10
- Year of training: 2017
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/a-modern-r-introduction-including-graphics-and-data-handling-january-17/)
- Date: 2017-01-18
- Duration (days): 3
```
---
2016-12_RTIDY1
^^^
**Introduction to R including graphics and data handling**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Command-line computing,badge-success`
```{dropdown} More details:
- Course name: Introduction to R including graphics and data handling
- Instructors: Bernd Klaus 
- Number of attendees: 10
- Year of training: 2016
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/a-modern-r-introduction-including-graphics-and-data-handling/)
- Date: 2016-12-14
- Duration (days): 3
```
---
2016-9_SWC1
^^^
**Software Carpentry**

{badge}`Programming languages,badge-success` {badge}`Command-line computing,badge-success` {badge}`Computational workflow management,badge-success` {badge}`Software project management,badge-success`
```{dropdown} More details:
- Course name: Software Carpentry
- Instructors: Luis Pedro Coelho,Toby Hodges,Thomas Schwarzl,Malvika Sharan,Mike Smith,Georg Zeller
- Number of attendees: 30
- Year of training: 2016
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/software-carpentry-workshop/)
- Date: 2016-09-19
- Duration (days): 3
```
---
2016-9_MATLAB1
^^^
**Introduction to MATLAB**

{badge}`Programming languages,badge-success`
```{dropdown} More details:
- Course name: Introduction to MATLAB
- Instructors: Karin Sasaki
- Number of attendees: N/A
- Year of training: 2016
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/matlab-fundamentals-2/)
- Date: 2016-09-12
- Duration (days): 4
```
---
2016-6_PYTH1
^^^
**Introduction to Python Programming**


```{dropdown} More details:
- Course name: Introduction to Python Programming
- Instructors: Toby Hodges
- Number of attendees: N/A
- Year of training: 2016
- Location: EMBL Monterotondo
- Format (In-person/Virtual/...): In-person
- Date: 2016-06-01
- Duration (days): 1
```
---
2016-4_PYIMG1
^^^
**Python Workshop: Image Processing**

{badge}`Image analysis,badge-success` {badge}`Image analysis,badge-warning`
```{dropdown} More details:
- Course name: Python Workshop: Image Processing
- Instructors: Jonas Hartmann,Karin Sasaki
- Number of attendees: 20
- Year of training: 2016
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/python-workshop-image-processing/)
- Date: 2016-04-06
- Duration (days): 2
```
---
2016-4_PYTH1
^^^
**Introduction to Python Programming**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Data management and curation,badge-success`
```{dropdown} More details:
- Course name: Introduction to Python Programming
- Instructors: Holger Dinkel,Toby Hodges,Karin Sasaki
- Number of attendees: N/A
- Year of training: 2016
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Date: 2016-04-01
- Duration (days): 1
```
---
2016-1_REGEXP1
^^^
**Introduction to Regular Expressions**

{badge}`Exploratory data analysis and visualisation,badge-warning` {badge}`Programming languages,badge-warning` {badge}`Command-line computing,badge-warning`
```{dropdown} More details:
- Course name: Introduction to Regular Expressions
- Instructors: Toby Hodges,Mike Smith
- Number of attendees: N/A
- Year of training: 2016
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://grp-bio-it-workshops.embl-community.io/regex-introduction/)
- Date: 2016-01-01
- Duration (days): 1
```
---
2015-12_MATLAB1
^^^
**MATLAB fundamentals**

{badge}`Programming languages,badge-success`
```{dropdown} More details:
- Course name: MATLAB fundamentals
- Instructors: Karin Sasaki
- Number of attendees: N/A
- Year of training: 2015
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/matlab-fundamentals/)
- Date: 2015-12-02
- Duration (days): 3
```
---
2015-11_SWC1
^^^
**Software Carpentry**

{badge}`Programming languages,badge-success` {badge}`Command-line computing,badge-success` {badge}`Computational workflow management,badge-success` {badge}`Software project management,badge-success`
```{dropdown} More details:
- Course name: Software Carpentry
- Instructors: Luis Pedro Coelho,Holger Dinkel,Toby Hodges
- Number of attendees: 30
- Year of training: 2015
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Date: 2015-11-01
- Duration (days): 1
```
---
2015-6_PYTH1
^^^
**Introduction To Python Programming**


```{dropdown} More details:
- Course name: Introduction To Python Programming
- Instructors: Holger Dinkel,Toby Hodges
- Number of attendees: N/A
- Year of training: 2015
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Date: 2015-06-01
- Duration (days): 1
```
---
2015-4_RTIDY1
^^^
**Basic R, Data Handling & Graphics**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Command-line computing,badge-success`
```{dropdown} More details:
- Course name: Basic R, Data Handling & Graphics
- Instructors: Bernd Klaus
- Number of attendees: 10
- Year of training: 2015
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/basic-r-data-handling-and-graphics/)
- Date: 2015-04-08
- Duration (days): 3
```
---
2015-3_RTIDY1
^^^
**Basic R & Data Handling**

{badge}`Exploratory data analysis and visualisation,badge-success` {badge}`Programming languages,badge-success` {badge}`Statistics and machine learning,badge-success` {badge}`Command-line computing,badge-success`
```{dropdown} More details:
- Course name: Basic R & Data Handling
- Number of attendees: 10
- Year of training: 2015
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Reference materials (URL/DOI): [link](https://bio-it.embl.de/events/basic-r-data-handling/)
- Date: 2015-03-16
- Duration (days): 3
```
---
2014-11_SWC1
^^^
**Software Carpentry**

{badge}`Programming languages,badge-success` {badge}`Command-line computing,badge-success` {badge}`Computational workflow management,badge-success` {badge}`Software project management,badge-success`
```{dropdown} More details:
- Course name: Software Carpentry
- Number of attendees: 30
- Year of training: 2014
- Location: EMBL Heidelberg
- Format (In-person/Virtual/...): In-person
- Date: 2014-11-01
- Duration (days): 1
```
---
````