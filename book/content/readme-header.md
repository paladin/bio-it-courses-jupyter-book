# Bio-IT courses

Welcome to Bio-IT courses catalogue. We are classifying past courses
according to the 
[competencies glossary](https://grp-bio-it.embl-community.io/blog/posts/2021-11-04-competencies/).
This glossary includes two types of terms: the **skills**, representing expertise, abilities to use tools, 
and the **topics**, matters of knowledge. Past courses are labeled with this terms and further
metadata such as the number of participants, names of trainers, training materials etc. 

Check the courses added so far in the **Training catalogue** section. 

