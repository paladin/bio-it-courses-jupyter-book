# Bio-IT courses

Welcome to Bio-IT courses catalogue. We are classifying past courses
according to the 
[competencies glossary](https://grp-bio-it.embl-community.io/blog/posts/2021-11-04-competencies/).
This glossary includes two types of terms: the **skills**, representing expertise, abilities to use tools, 
and the **topics**, matters of knowledge. Past courses are labeled with this terms and further
metadata such as the number of participants, names of trainers, training materials etc. 

Check the courses added so far in the 
[**Training catalogue**](https://paladin.embl-community.io/bio-it-courses-jupyter-book/content/catalogue.html#) section.

Below, some statistics of the year of training for which we have complete data. 
Find out more in the 
[**Catalogue statistics**](https://paladin.embl-community.io/bio-it-courses-jupyter-book/chapters/statistics.html) section.

## Summary statistics
Years: 2018, 2019, 2020, 2021
- **Number of courses**: 92
- **Instructors**: 86
- **Number of participants**: 2121
