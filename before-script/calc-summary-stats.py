import json
import os
import pandas as pd

# get absolute path
file_path = os.path.dirname(__file__).strip('before-script')


mjson = open(file_path + "data/done-data.mjson").readlines()

# get header form README
readme = open(file_path + "book/README.md", "r")
readmetext = readme.read().split("## Summary statistics")

complete_years = range(2018, 2022)
courses_object = [json.loads(line) for line in mjson]
courses_object_filtered = [c for c in courses_object if c['metadata']['Year of training'] in complete_years]

# flatten mjson to data table
course_keys = courses_object_filtered[0].keys()  # 'course-ID', 'metadata', 'classification'
metadata_keys = courses_object_filtered[0]['metadata'].keys()
classification_keys = courses_object_filtered[0]['classification'].keys()
topic_keys = courses_object_filtered[0]['classification']['Beginner'].keys()

# load into dataframe and copy to file
dataframe_input = {'course-ID': [obj['course-ID'] for obj in courses_object_filtered]}
for k in metadata_keys:
    dataframe_input[k] = [obj['metadata'][k] for obj in courses_object_filtered]
columns_to_int = [
    'Year of training', 'Duration (days)'
]
for c in classification_keys:
    for t in topic_keys:
        columns_to_int.append(c + ' ' + t)
        dataframe_input[c + ' ' + t] = [
            obj['classification'][c][t] if obj['classification'][c][t] != ' ' else 0 for obj in courses_object_filtered
        ]
# Convert the dictionary into DataFrame
df = pd.DataFrame(dataframe_input)
df = df.fillna(value=0)
df[columns_to_int] = df[columns_to_int].astype(int)
df.to_csv(file_path + "data/done-data.tsv", sep="\t")

outputstring = readmetext[0]
outputstring += "## Summary statistics\n"
outputstring += "Years: " + ', '.join([str(y) for y in complete_years]) + "\n"
stats = {
    "Number of courses" : len(courses_object_filtered),
    "Instructors": len(set([n.lstrip() for c in courses_object_filtered if c['metadata']['Instructors']
                            for n in c['metadata']['Instructors'].split(',')])),
    "Number of participants" : sum([int(c['metadata']['Number of attendees'])
                                    for c in courses_object_filtered
                                    if c['metadata']['Number of attendees'] != 'N/A'])
}
for s in stats:
    outputstring += "- **{}**: {}\n".format(s, stats[s])

# overwrite README with full file
output = open(file_path + "book/README.md", "w+")
output.write(outputstring)


print("calc-summary-stats successful")