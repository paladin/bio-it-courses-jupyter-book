import json
import os
import pandas as pd
import datetime

# get absolute path
file_path = os.path.dirname(__file__).strip('before-script')


def formatDate(dates):
    # months and days could be written as: 1 or: 1,2
    # default first if None
    return [1] if not dates else [int(dates)] \
        if ',' not in str(dates) else [int(x) for x in dates.strip(' ').split(',')]


def formatFloat(fl):
    return int(fl) if fl else "N/A"


inputfiles = os.listdir(file_path + "data/Done")
outputmjson = open(file_path + "data/done-data.mjson", "w+")
for flname in inputfiles:
    fl = pd.read_excel(file_path + "data/Done/"+flname, header=None, index_col=0)
    flclassification = pd.read_excel(file_path + "data/Done/"+flname, 'Course Skills', index_col=0, header=1)
    fljson = json.loads(fl.to_json())
    # correct \n separator in 'Reference materials (URL/DOI)'
    references = fljson['1']['Reference materials (URL/DOI)']
    fljson['1']['Reference materials (URL/DOI)'] = references.split('\n') if references else ''
    # format attendees number
    fljson['1']['Number of attendees'] = formatFloat(fljson['1']['Number of attendees'])
    # format date
    days = formatDate(fljson['1']['Dates'])
    months = formatDate(fljson['1']['Month'])
    fljson['1']['Year of training'] = formatFloat(fljson['1']['Year of training'])
    fljson['1']['Date'] = datetime.date(year=int(fljson['1']['Year of training']), month=months[0], day=days[0]).isoformat()
    fljson['1']['Duration (days)'] = len(days)
    obj = {
        "course-ID": flname.strip(".xlsx"),
        "metadata": fljson["1"],
        "classification": json.loads(flclassification.to_json())
    }
    outputmjson.write(json.dumps(obj)+'\n')

print("build-database successful")
