import json
import copy
import os

# get absolute path
file_path = os.path.dirname(__file__).strip('before-script')

### HOME PAGE
# managed by clac-summary-stats

### CATALOGUE
mjson = open(file_path + "data/done-data.mjson").readlines()
output = open(file_path + "book/content/catalogue.md", "w+")

# add header
output.write(open(file_path + "book/content/catalogue-header.md").read())

# style according to https://jupyterbook.org/content/content-blocks.html?highlight=card
courses_object = [json.loads(line) for line in mjson]
courses_object.sort(key=lambda item: item["metadata"]['Date'], reverse=True)
for obj in courses_object:
    # open card, card heading
    outlines = "{}\n^^^\n".format(obj["course-ID"])
    # title
    outlines += "**{}**\n".format(obj["metadata"]["Course name"])
    # classification
    # add badges as: {badge}`example-badge,badge-primary`
    badges, badgestr = [], "{badge}`example-badge,badge-primary`"
    for level in obj["classification"]:
        # level determines badges color
        color = 'success' if level == 'Beginner' else 'warning' if level == 'Intermediate' else 'danger'
        for topic in obj["classification"][level]:
            if obj["classification"][level][topic]:
                # this means that some time of the course is dedicated to the topic
                thisbadgestr = copy.copy(badgestr).replace('example-badge', topic).replace('primary', color)
                badges.append(thisbadgestr)
    outlines += "\n" + ' '.join(badges) + "\n"
    # badges = [x for x]
    # open dropdown
    outlines += "```{dropdown} More details:\n"
    for m_obj in obj["metadata"]:
        if obj["metadata"][m_obj]:
            if m_obj == "Reference materials (URL/DOI)":
                outlines += "- {}: ".format(m_obj) + \
                            ", ".join(["[link]({})".format(l) for l in obj["metadata"][m_obj]]) + \
                            "\n"
            elif m_obj in ['Dates', 'Month']:
                pass
            else:
                outlines += "- {}: {}\n".format(m_obj, obj["metadata"][m_obj])
    # close dropdown
    outlines += "```\n"
    # close card
    outlines += "---\n"
    output.write(outlines)
output.write("````")

print("build-website-content successful")